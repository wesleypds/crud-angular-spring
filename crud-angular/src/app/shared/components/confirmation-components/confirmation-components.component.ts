import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-confirmation-components',
  templateUrl: './confirmation-components.component.html',
  styleUrls: ['./confirmation-components.component.scss']
})
export class ConfirmationComponentsComponent {

  constructor(
    public dialogRef: MatDialogRef<ConfirmationComponentsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: string,
  ) {}

  onConfirm(result: boolean): void {
    this.dialogRef.close(result);
  }

}
