import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmationComponentsComponent } from './confirmation-components.component';

describe('ConfirmationComponentsComponent', () => {
  let component: ConfirmationComponentsComponent;
  let fixture: ComponentFixture<ConfirmationComponentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfirmationComponentsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ConfirmationComponentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
