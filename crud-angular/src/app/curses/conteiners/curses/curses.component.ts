import { CursesService } from '../../services/curses.service';
import { Component } from '@angular/core';
import { Curse } from '../../model/curse';
import { Observable, catchError, of } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { ErrorDialogComponent } from 'src/app/shared/components/error-dialog/error-dialog.component';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ConfirmationComponentsComponent } from 'src/app/shared/components/confirmation-components/confirmation-components.component';

@Component({
  selector: 'app-curses',
  templateUrl: './curses.component.html',
  styleUrls: ['./curses.component.scss']
})
export class CursesComponent {
  curses$: Observable<Curse[]> | null = null;


  constructor(
    private cursesService: CursesService,
    public dialog: MatDialog,
    private router: Router,
    private snackBar: MatSnackBar,
    private route: ActivatedRoute
    ) {
    this.refresh();
  }

  refresh() {
    this.curses$ = this.cursesService.list().pipe(
      catchError(error => {
        this.onError('Error ao carregar cursos.')
        return of([])
      })
    );
  }

  onError(errorMsg: string) {
    this.dialog.open(ErrorDialogComponent, {
      data: errorMsg
    });
  }

  onAdd() {
    this.router.navigate(['new'], {relativeTo: this.route});
  }

  onEdit(curse: Curse) {
    this.router.navigate(['edit', curse._id], {relativeTo: this.route});
  }

  onDelete(curse: Curse) {
    const dialogRef = this.dialog.open(ConfirmationComponentsComponent, {
      data: 'Tem certeza que deseja remover este curso?'
    });
    dialogRef.afterClosed().subscribe((result: boolean) => {
      if (result) {
        this.cursesService.remove(curse._id).subscribe(() => {
          this.refresh();
          this.onSucessSnackBar();
          },
          () => this.onError('Erro ao tentar remover o curso.')
        );
      }
    });

  }

  private onSucessSnackBar() {
    this.snackBar.open('Curso removido com sucesso.', '', { duration: 5000, verticalPosition: 'top', horizontalPosition: 'center' });
  }

}
