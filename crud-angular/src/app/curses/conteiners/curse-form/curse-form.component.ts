import { Component } from '@angular/core';
import { FormControl, NonNullableFormBuilder, Validators } from '@angular/forms';
import { CursesService } from '../../services/curses.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Curse } from '../../model/curse';
import { Observable, map, startWith } from 'rxjs';

@Component({
  selector: 'app-curse-form',
  templateUrl: './curse-form.component.html',
  styleUrls: ['./curse-form.component.scss']
})
export class CurseFormComponent {

  options: Curse[] = [];

  myControl = new FormControl<string | Curse>('');

  filteredOptions: Observable<Curse[]>;

  form = this.formBuilder.group({
    _id: [''],
    name: ['', [Validators.required,
    Validators.minLength(5),
    Validators.maxLength(100)]],
    category: ['', [Validators.required]]
  });

  constructor(
    private formBuilder: NonNullableFormBuilder,
    private service: CursesService,
    private snackBar: MatSnackBar,
    private location: Location,
    private route: ActivatedRoute) {

    const curse: Curse = this.route.snapshot.data['curse'];

    this.autoComplete();

    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => {
        const name = typeof value === 'string' ? value : value?.name;
        return name ? this._filter(name as string) : this.options.slice();
      }),
    );

    this.form.setValue(
      {
        _id: curse._id,
        name: curse.name,
        category: curse.category,
      }
    );

  }

  displayFn(user: Curse): string {
    return user && user.name ? user.name : '';
  }

  private _filter(name: string): Curse[] {
    const filterValue = name.toLowerCase();

    return this.options.filter(option => option.name.toLowerCase().includes(filterValue));
  }

  public onSubmit() {
    this.service.save(this.form.value).subscribe(result => this.onSucess(), error => this.onError());
  }

  public onCancel() {
    this.location.back();
  }

  private onSucess() {
    this.snackBar.open('Curso salvo com sucesso.', '', { duration: 5000 });
    this.onCancel();
  }

  public autoComplete() {
    this.service.list().subscribe(response => {
      this.options = response;
    })
  }

  private onError() {
    this.snackBar.open('Erro ao salvar curso.', '', { duration: 5000 });
  }

  getErrorMessage(fieldName: string) {
    const field = this.form.get(fieldName);

    if (field?.hasError('required')) {
      return 'Campo obrigatório.';
    }

    if (field?.hasError('minlength')) {
      const requiredLength = field.errors ? field.errors['minlength']['requiredLength'] : 5;
      return `Tamanho mínimo precisa ser de ${requiredLength} caracteres.`;
    }

    if (field?.hasError('maxlength')) {
      const requiredLength = field.errors ? field.errors['maxlength']['requiredLength'] : 100;
      return `Tamanho máximo excedido de ${requiredLength} caracteres.`;
    }

    return 'Campo inválido.'
  }

}
