import { Injectable } from '@angular/core';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { CursesService } from '../services/curses.service';
import { Curse } from '../model/curse';

@Injectable({
  providedIn: 'root'
})
export class CurseResolver implements Resolve<Curse> {

  constructor(private service: CursesService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Curse> {
    if (route.params && route.params['id']) {
      return this.service.loadById(route.params['id']);
    }
    return of({_id: '', name: '', category: ''});
  }
}
