import { TestBed } from '@angular/core/testing';

import { CurseResolver } from './curse.resolver';

describe('CurseResolver', () => {
  let resolver: CurseResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(CurseResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
