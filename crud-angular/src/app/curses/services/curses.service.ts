import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Curse } from '../model/curse';
import { first, tap } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CursesService {

  private readonly API = '/api/cursos'

  constructor(private httpClient : HttpClient) { }

  list() {
    return this.httpClient.get<Curse[]>(this.API);
  }

  loadById(id: string) {
    return this.httpClient.get<Curse>(`${this.API}/${id}`);
  }

  save(record: Partial<Curse>) {
    if (record._id) {
      return this.update(record);
    }
    return this.create(record);
  }

  private create(record: Partial<Curse>) {
    return this.httpClient.post<Curse>(this.API, record);
  }

  private update(record: Partial<Curse>) {
    return this.httpClient.put<Curse>(`${this.API}/${record._id}`, record);
  }

  remove(id: string) {
    return this.httpClient.delete(`${this.API}/${id}`);
  }
}
