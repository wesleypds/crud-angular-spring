import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { AngularMaterialModule } from '../shared/angular-material/angular-material.module';
import { CursesRoutingModule } from './curses-routing.module';
import { CursesComponent } from './conteiners/curses/curses.component';
import { SharedModule } from '../shared/shared.module';
import { CurseFormComponent } from './conteiners/curse-form/curse-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CursesListComponent } from './components/curses-list/curses-list.component';

@NgModule({
  declarations: [
    CursesComponent,
    CurseFormComponent,
    CursesListComponent
  ],
  imports: [
    CommonModule,
    CursesRoutingModule,
    AngularMaterialModule,
    SharedModule,
    ReactiveFormsModule
  ]
})
export class CursesModule { }
