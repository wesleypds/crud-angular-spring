import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CursesComponent } from './conteiners/curses/curses.component';
import { CurseFormComponent } from './conteiners/curse-form/curse-form.component';
import { CurseResolver } from './guards/curse.resolver';

const routes: Routes = [
  { path: '', component: CursesComponent },
  { path: 'new', component: CurseFormComponent, resolve: { curse: CurseResolver } },
  { path: 'edit/:id', component: CurseFormComponent, resolve: { curse: CurseResolver } }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CursesRoutingModule { }
