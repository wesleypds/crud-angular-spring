import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Curse } from '../../model/curse';

@Component({
  selector: 'app-curses-list',
  templateUrl: './curses-list.component.html',
  styleUrls: ['./curses-list.component.scss']
})
export class CursesListComponent {

  @Input() curses: Curse[] = [];
  @Output() add = new EventEmitter(false);
  @Output() edit = new EventEmitter(false);
  @Output() remove = new EventEmitter(false);

  readonly displayedColumns = ['name', 'category', 'actions'];

  onAdd() {
    this.add.emit(true);
  }

  onEdit(curse: Curse) {
    this.edit.emit(curse);
  }

  onDelete(curse: Curse) {
    this.remove.emit(curse);
  }

}
