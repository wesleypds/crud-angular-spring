export interface Curse {
  _id: string;
  name: string;
  category: string;
}
