package com.wesley.crudspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wesley.crudspring.model.Curso;

public interface CursoRepository extends JpaRepository<Curso, Long> {
    
}
