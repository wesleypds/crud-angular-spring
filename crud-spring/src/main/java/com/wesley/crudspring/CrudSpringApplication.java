package com.wesley.crudspring;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.wesley.crudspring.enums.Category;
import com.wesley.crudspring.model.Aula;
import com.wesley.crudspring.model.Curso;
import com.wesley.crudspring.repository.CursoRepository;

@SpringBootApplication
public class CrudSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudSpringApplication.class, args);
	}

	@Bean
	CommandLineRunner initDatabase(CursoRepository cursoRepository) {
		return args -> {
			cursoRepository.deleteAll();

			Curso c = new Curso();
			c.setName("Angular com Spring");
			c.setCategory(Category.FRONTEND);

			c.getListAulas().add(new Aula("Introdução", "Nb4uxLxdvxo", c));

			c.getListAulas().add(new Aula("Angular", "Nb4uxLxdvxo", c));

			cursoRepository.save(c);
		};
	}

}
