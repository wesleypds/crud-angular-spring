package com.wesley.crudspring.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import com.wesley.crudspring.dto.CursoDTO;
import com.wesley.crudspring.dto.mapper.CursoMapping;
import com.wesley.crudspring.exception.RecordNotFoundException;
import com.wesley.crudspring.repository.CursoRepository;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;

@Validated
@Service
public class CursoService {

    private final CursoRepository cursoRepository;
    private final CursoMapping cursoMapping;

    public CursoService(CursoRepository cursoRepository, CursoMapping cursoMapping) {
        this.cursoRepository = cursoRepository;
        this.cursoMapping = cursoMapping;
    }

    public List<CursoDTO> list() {
        return cursoRepository.findAll()
        .stream()
        .map(cursoMapping :: toDTO)
        .collect(Collectors.toList());
    }

    public CursoDTO findById(@NotNull @Positive Long id) {
        return cursoRepository.findById(id)
        .map(cursoMapping :: toDTO)
        .orElseThrow(
            () -> new RecordNotFoundException(id)
        );
    }

    public CursoDTO create(@Valid @NotNull CursoDTO recordCurso) {
        return cursoMapping.toDTO(cursoRepository.save(cursoMapping.toEntity(recordCurso)));
    }

    public CursoDTO update(@NotNull @Positive Long id, @Valid @NotNull CursoDTO curso) {
        return cursoRepository.findById(id)
        .map(recordCurso -> {
            recordCurso.setName(curso.name());
            recordCurso.setCategory(cursoMapping.converterCategoryValue(curso.category()));
            return cursoMapping.toDTO(cursoRepository.save(recordCurso));
        }).orElseThrow(
            () -> new RecordNotFoundException(id)
        );
    }

    public void delete(@NotNull @Positive Long id) {

        cursoRepository.delete(cursoRepository.findById(id).orElseThrow(
                () -> new RecordNotFoundException(id)
            )
        );

    }

}
