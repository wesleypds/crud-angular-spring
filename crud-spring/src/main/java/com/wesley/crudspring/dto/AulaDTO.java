package com.wesley.crudspring.dto;

public record AulaDTO(
    Long id,
    String name,
    String ytUrl
) {
    
}
