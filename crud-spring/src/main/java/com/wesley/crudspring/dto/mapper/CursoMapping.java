package com.wesley.crudspring.dto.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.hibernate.mapping.Collection;
import org.springframework.stereotype.Component;

import com.wesley.crudspring.dto.AulaDTO;
import com.wesley.crudspring.dto.CursoDTO;
import com.wesley.crudspring.enums.Category;
import com.wesley.crudspring.model.Curso;

@Component
public class CursoMapping {
    
    public CursoDTO toDTO(Curso curso) {
        if (curso == null) {
            return null;
        }
        List<AulaDTO> listAulasDTO = curso.getListAulas()
                    .stream()
                    .map(aula -> new AulaDTO(aula.getId(), aula.getNome(), aula.getYtUrl()))
                    .collect(Collectors.toList()); 
        return new CursoDTO(curso.getId(), curso.getName(), curso.getCategory().getValue(),
                    listAulasDTO);
    }

    public Curso toEntity(CursoDTO cursoDTO) {
        if (cursoDTO == null) {
            return null;
        }
        Curso curso = new Curso();
        if (cursoDTO.id() != null) {
            curso.setId(cursoDTO.id());
        }
        curso.setName(cursoDTO.name());
        curso.setCategory(converterCategoryValue(cursoDTO.category()));
        return curso;
    }

    public Category converterCategoryValue(String value) {
        if (value == null) {
            return null;
        }
        return switch (value) {
            case "Front-End" -> Category.FRONTEND;
            case "Back-End" -> Category.BACKEND;
            default -> throw new IllegalArgumentException("Categoria Inválida: " + value);
        };
    }

}
