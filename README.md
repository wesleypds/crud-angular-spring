# Crud Angular/Spring

## Angular

**npm** - é um gerenciador de pacotes que faz parte do node.js, que ajuda no gerenciamento de pacotes e dependências para que se possa trabalhar com o angular.

**angular cli** - é uma ferramenta do angular para terminal que ajuda na criação de projetos, fazer build, criação de componentes e etc.

**npm i -g @angular/cli** - é o comando para instalar a ferramenta angular cli na versão mais recente.

**ng version** - é o comando para verificar a versão da ferramenta angular cli.

**ng new 'nome do projeto'** - é o comando para criar um novo projeto angular.

**ng serve** - é o comando para executar o projeto.

Sempre que o projeto é executado, se estiver funcionando tudo perfeitamente, será disponível um link para a visualização do projeto no navegador.

A compilação do angular é incremental, sempre que o projeto for salvo depois de uma alteração, será compilado e executado automaticamente no navegador.

**ng add @angular/material** - é o comando para adicionar o angular material no projeto, onde já se faz todas as configurações necessárias nos arquivos do projeto.

Antes de adicionar um componente ao projeto, primeiro adiciona o módulo desse componente.

**ng generate module 'nome do módulo' --routing** - é um comando para fazer a criação de um módulo, roteamento e importação desse módulo, ajudando na organização do projeto. Módulos tem a finalidade semelhante de um pacote em um projeto Java.

**ng generate component 'nome do módulo/nome do componente'** - é o comando para gerar um componente em um módulo.

**ng generate interface 'nome da interface'** - é o comando para gerar uma interface no projeto.

**npm run start** - é o comando para startar o projeto depois que a configuração da api for feita.

**ng update** - é o comando para verificar as atualizações que devem ser feitas no projeto.

**ng generate service 'nome do serviço'** - é o comando para gerar um serviço no projeto.

**ng generate resolver 'nome do resolver'** - é o comando para gerar um guarda de rota resolver.

Quando for criar um módulo e seus componentes, para que ele seja renderizado na página é preciso configurar a rota de seus componentes no arquivo de configuração de rotas do módulo, em seguida, configurar a rota do módulo no arquivo de rota global. E no final chamar a tag router-outlet no html global para que o módulo seja renderizado.

Para que seja usado o angular material, além de instalar ele, também é preciso importar o angular material no arquivo de estilo global do projeto e incluir os mixin.

Mixin é um conceito semelhante ao conceito de herança, só que mais maleável na utilização. É um conceito onde uma classe poderá utilizar métodos e atributos de outra classe, fazendo a reutilização de códigos.

Para criar qualquer componente dentro de um módulo, é preciso antes importar o componente no arquivo do módulo e adicionar o componente no imports.

O angular tem uma diretiva ngFor, que é uma estrutura de repetição usada para percorrer arrays/listas. É semelhante com foreach do Java.

Componentes inteligentes são os componentes que fazem algum tipo de integração com outro componente da aplicação.

Componentes de apresentação são os componentes que ficarão encarregados do input e output da aplicação.

Na classe serviço, é onde tem toda a manipulação dos dados e as regras de negócio, já no componente terá apenas os códigos para que ele renderize na tela e validações básicas.

Para atualizar o Angular não pode ter pendências a ser commitadas.

## Spring

Para criar um projeto Spring no vscode basta acionar o comando 'Ctrl + Shift + p' para abrir a paleta do vscode e procurar pela opção spring initializr mais o gerenciador de pacotes usado, com isso é só completar as etapas de configuração que o projeto estará pronto.

Outra forma de criar um projeto Spring é pelo site http://start.spring.io/.

O Spring Boot é uma ferramenta do framework Sprig para facilitar na criação e configuração do projeto.

H2 é um banco de dados em memória utilizado no Spring, mas apenas para fins de teste ou estudo.

O arquivo de propriedades da aplicação serve para configurar a aplicação, mais especificamente o acesso ao banco de dados.

http://localhost:8080/h2-console/ é o link para acessar o banco de dados H2. Ele só funcionará em tempo de execução.

#### Anotações:

**@RestController** - é a anotação para indicar que na classe onde ela está sendo usada terá uma URL para acesso da API.

**@RequestMapping("/api/cursos")** - é a anotação que define a URL onde a API será acessada.

**@GetMapping** - é a anotação que indica que um certo método terá o chamado GET.

**@PostMapping** - é a anotação que indica que um certo método terá o chamado POST.

**@PutMapping** - é a anotação que indica que um certo método terá o chamado PUT.

**@DeleteMapping** - é a anotação que indica que um certo método terá o chamado DELETE.

**@Data** - é a anotação do lombok que indica que uma classe terá todos os métodos Getters, Setters e etc.

**@Entity** - é a anotação que indica que uma classe será uma entidade em um banco de dados.

**@Table(name = "nome da tabela")** - caso já tenha uma tabela criada no banco só que com o nome diferente, essa anotação indicará que a entidade na qual ela foi declarada pertencerá a tabela da anotação.

**@Id** - é a anotação que indica que o atributo será uma chave-primária.

**@GeneratedValue(strategy = GenerationType.AUTO)** - é a anotação que indica que o id será gerado automaticamente pelo banco de dados. Existe outras propriedades da anotação.

**@Column(length = 200, nullable = false)** - é a anotação que indica o tamanho máximo de uma coluna e se nela poderá ter o valor nulo. Existe outras propriedades da anotação.

**@Repository** - é a anotação que indica que uma interface de repositório herdará interfaces do spring data para facilitar o acesso ao banco de dados.

**@AllArgsConstructor** - é a anotação que criará um construtor automaticamente para uma classe.

**@Bean** - é a anotação que gerencia o siclo de vida de um determinado trecho de código assim que a aplicação subir.

**@JsonProperty(nome da variavel)** - é a notação que transforma a variável da api para o nome da variável do serviço que está consumindo esta api.

**@RequestBody** - é a anotação que manipula automaticamente o conteúdo de uma request.

**@ResponseStatus(code = *HttpStatus*.CREATED)** - é a anotação para retornar o código correto da chamada. Neste exemplo está retornando o 201.

**@PathVariable** - é a anotação que irá indicar que uma variável faz parte da URL.

**@NotNull** - é a anotação para validar que um atributo não seja nulo ou vazio.

**@NotBlank** - é a anotação para validar que pelo menos tenha um caractere que não seja espaço.

**@Length** - é a anotação para estipular o tamanho min e max dos dados que vão ser inseridos em um campo.

**@Valid** - é a anotação para validar se as informações recebidas de uma instância são válidas de acordo com o que foi configurado na classe de entidade.

**@Positive** - é a anotação para validar se um número vai ser positivo.

**@Validated** - essa anotação é usada para que outras anotações de validação funcione. 

**@SQLDelete** - é a anotação para passar um query específica quando for chamado o método HTTP delete.

**@RestControllerAdvice** - é a anotação que indica que uma classe é um controller advice.

#### Propriedades do banco

**spring.datasource.url=jdbc:h2:mem:testedb** - propriedade que configura o nome do banco de dados.

**spring.datasource.driverClassName=org.h2.Driver** - propriedade que configura o driver do banco de dados.

**spring.datasource.username=sa** - propriedade que configura o nome de usuário.

**spring.datasource.password=password** - propriedade que configura a senha de usuário.

**spring.jpa.database-platform=org.hibernate.dialect.H2Dialect** - propriedade que configura o dialeto do banco de dados que será utilizado.

**spring.jpa.show-sql=true** - propriedade para mostrar o sql gerado.
